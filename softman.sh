#sudo chmod -R 755 scripts
#sudo chmod -R 755 tools

cd scripts

if [ -z "$1" ]
then
	echo ""
	echo "Software Manager"
	echo ""
	echo "Server Software:"
	echo "1) Joofthan Server (Wildfly)"
	echo "2) Nextcloud"
	echo ""
	echo "Software:"
	echo "3) JetBrains Toolbox"
	echo "4) Nemo file manager"
	echo "5) Android Studio"
	echo "6) Flutter"
	echo "7) Unity Hub"
	echo "8) Visual Studio Code"
	echo "9) Docker"
	echo ""
	echo "Configuration:"
	echo "a) VS Code Theme"
	echo "b) Git Config"
	echo "c) Nautilus: Add empty-file-option"
	echo "d) Docker config"
	echo "e) Clone all Joofthan Repos"
	echo ""
	echo "q) Exit"
	read -p "Option: " selected
else
	selected="$1"
	combined="$1$2"
fi

if [ $selected == '1' ] || [ $combined == 'installjoofthan-server' ]; then
	./wildfly.sh
elif [ $selected == '2' ] || [ $combined == 'installnextcloud' ]; then
	./nextcloud.sh


elif [ $selected == '3' ] || [ $combined == 'installintellij' ]|| [ $combined == 'installtoolbox' ]|| [ $combined == 'installjetbrains-toolbox' ]; then
	./jetbrains-toolbox.sh
elif [ $selected == '4' ] || [ $combined == 'installnemo' ]; then
	./nemo.sh
elif [ $selected == '5' ] || [ $combined == 'installandroid-studio' ]; then
	./android-studio.sh
elif [ $selected == '6' ] || [ $combined == 'installflutter' ]; then
	./flutter.sh
elif [ $selected == '7' ] || [ $combined == 'installunity' ] || [ $combined == 'installunity-hub' ]; then
	./unity.sh
elif [ $selected == '8' ] || [ $combined == 'installcode' ]; then
	./code.sh
elif [ $selected == '9' ] || [ $combined == 'installdocker' ]; then
	./docker.sh
fi


cd config
if [ $selected == 'a' ]; then
	./code-theme.sh
elif [ $selected == 'b' ] || [ $selected == 'git' ]  || [ $selected == 'git-config' ]; then
	./git-config.sh
elif [ $selected == 'c' ]; then
	./nautilus-empty-file.sh
elif [ $selected == 'd' ] || [ $selected == 'docker' ] || [ $selected == 'docker-config' ]; then
	./docker-config.sh $@
elif [ $selected == 'e' ]; then
	./clone-all-joofthan-repos.sh
fi