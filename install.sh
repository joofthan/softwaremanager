SOFTMANPATH=$HOME/softwaremanager

if [ -d "$SOFTMANPATH" ]; then
    #already installed
    cd $SOFTMANPATH/softwaremanager
    if [ -z "$1" ]
        then
        echo "Nach Updates suchen..."
        git pull
        echo ""
    fi
    ./softman.sh $@
else
    #do install
    sudo apt install git -y
    mkdir -p $SOFTMANPATH
    cd $SOFTMANPATH
    git clone https://gitlab.com/joofthan/softwaremanager.git
    git config pull.rebase true
    echo "adding softman to '~/.bashrc'"
    echo "" >> ~/.bashrc
    echo "alias softman=\"$SOFTMANPATH/softwaremanager/install.sh\"" >> ~/.bashrc
    echo ""
    echo "try typing: softman"
fi
