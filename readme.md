# Softwaremanager
## Install Softwaremanager
```bash
curl -sSL https://gitlab.com/joofthan/softwaremanager/-/raw/master/install.sh | bash && . ~/.bashrc
```
## Useage

```
softman
```

#### Download package:

```
softman install xxx
```

#### Available packages:
- android-studio
- code
- docker
- flutter
- jetbrains-toolbox
- joofthan-server
- nemo
- nextcloud
- unity

### Manage Docker Daemon
```
softman docker start
softman docker stop
softman docker enable
softman docker disable
```
