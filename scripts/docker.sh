if [ -n "$(command -v pacman)" ]; then
    sudo pacman -Syu
    sudo pacman -S docker
    sudo systemctl enable docker
    sudo systemctl start docker.service
else
    apt install curl
    curl -fsSL https://get.docker.com | bash
fi
