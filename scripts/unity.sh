mkdir ~/softwaremanager/unity

curl -o ~/softwaremanager/unity/UnityHub.AppImage https://public-cdn.cloud.unity3d.com/hub/prod/UnityHub.AppImage
sudo chmod +x ~/softwaremanager/unity/UnityHub.AppImage

curl -o ~/softwaremanager/unity/icon.png https://www.iconfinder.com/icons/4691514/download/png/512

cd ../tools
./shortcut.sh UnityHub unity/UnityHub.AppImage unity/icon.png