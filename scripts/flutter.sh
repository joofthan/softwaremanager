if [ -d "$HOME/softwaremanager/flutter" ]; then
  echo ""
  echo "Flutter already installed."
  echo "Flutter Path: ~/softwaremanager/flutter/bin"
  echo "Dart SDK Path: ~/softwaremanager/flutter/bin/cache/dart-sdk"
  exit
fi

mkdir ~/softwaremanager/
cd ~/softwaremanager/

sudo apt install git gradle -y
git clone https://github.com/flutter/flutter.git
echo "export PATH=\"\$PATH:`pwd`/flutter/bin\"" >> $HOME/.bashrc
export PATH="$PATH:`pwd`/flutter/bin"

flutter doctor