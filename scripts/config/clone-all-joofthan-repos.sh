mkdir ~/joofthan
cd ~/joofthan


git clone https://gitlab.com/joofthan/automatictvrecorder.git
git clone https://gitlab.com/joofthan/empty-java-webapp.git
git clone https://gitlab.com/joofthan/empty-java-webapp-jee8.git
git clone https://gitlab.com/joofthan/linux-server.git
git clone https://gitlab.com/joofthan/maven-poms.git
git clone https://gitlab.com/joofthan/prettyui.git
git clone https://gitlab.com/joofthan/remote-browser.git


git clone https://gitlab.com/joofthan/softwaremanager.git


mkdir popupblocker
cd popupblocker
git clone https://gitlab.com/popupblocker/firefox.git

echo ""
echo "Cloned to ~/joofthan"
