if [ -z "$1" ]
then
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo ""
    echo "Docker config"
    echo ""
    echo "1) start Docker Daemon"
    echo "2) stop Docker Daemon"
    echo "3) enable autorun Docker Daemon"
    echo "4) disable autorun Docker Daemon"
    if ! command -v docker &> /dev/null
    then
        echo ""
        echo "5) install Docker"
    fi
    echo ""
    echo "q) Exit"

    read -p "Option: " selected
else
    selected=$2
fi

if [ $selected == '1' ] || [ $selected == 'start' ]; then
    sudo systemctl start docker.service
elif [ $selected == '2' ] || [ $selected == 'stop' ]; then
	sudo systemctl stop docker.service
elif [ $selected == '3' ] || [ $selected == 'enable' ]; then
    sudo systemctl enable docker
    sudo systemctl start docker.service
elif [ $selected == '4' ] || [ $selected == 'disable' ]; then
    sudo systemctl disable docker
fi