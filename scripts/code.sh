if [ -n "$(command -v code)" ]; then
    echo "Visual Studio Code already installed"
    exit
fi


if [ -n "$(command -v apt)" ]; then
    cd /tmp
    apt install wget
    wget -O code.deb https://go.microsoft.com/fwlink/?LinkID=760868
    apt install ./code.deb
    rm code.deb 
elif [ -n "$(command -v dnf)" ]; then
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    sudo dnf check-update
    sudo dnf install code
elif [ -n "$(command -v zypper)" ]; then
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo zypper addrepo https://packages.microsoft.com/yumrepos/vscode vscode
    sudo zypper refresh
    sudo zypper --non-interactive install code
else
    echo apt, dnf, zypper not found.
fi
