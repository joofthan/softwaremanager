echo "$2"
name="$1"
path="$2"
iconpath="$3"
filename=~/.local/share/applications/$name.desktop

echo "[Desktop Entry]" > $filename
echo "Version=1.0" >> $filename
echo "Type=Application" >> $filename
echo "Terminal=false" >> $filename
echo "Exec=\"$HOME/softwaremanager/$path\" %f" >> $filename
echo "Name=$name" >> $filename
echo "Comment=$name" >> $filename
echo "Icon=$HOME/softwaremanager/$iconpath" >> $filename
